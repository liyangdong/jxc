package com.atguigu.jxc;

import com.atguigu.jxc.service.SupplierService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 17:10
 * @Version:1.0
 * @Description
 **/
@SpringBootTest
public class SplitTest {
    @Autowired
    SupplierService supplierService;
    @Test
    public void test() {
        //ids: 11,12,13
        String ids = "1,2,3,4";
        String[] split = ids.split(",");
        List<String> idList = new ArrayList<>();
        Arrays.stream(split).forEach(item -> idList.add(item));
        System.out.println(idList);
    }
}
