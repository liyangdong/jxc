package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 19:21
 * @Version:1.0
 * @Description
 **/
@RestController
public class UnitController {
    @Autowired
    UnitService unitService;


    /**
     * 查询所有商品单位
     * @return
     */
    //：http://localhost:8080/unit/list
    @PostMapping("/unit/list")
    public Map<String, Object> getGoodsUnit() {
        Map<String, Object> map = unitService.getUnit();
        return map;
    }

}
