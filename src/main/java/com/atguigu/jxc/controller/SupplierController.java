package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 15:06
 * @Version:1.0
 * @Description
 **/
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    /**
     * 分页查询供应商
     */
    //http://localhost:8080/supplier/list
    @PostMapping("/list")
    public Map<String, Object> listSupplier(@RequestParam(required = false) Integer page,
                                            @RequestParam(required = false) Integer rows,
                                            @RequestParam(required = false) String supplierName) {
        Map<String, Object> map = supplierService.listSupplier(page, rows, supplierName);
        return map;
    }

    /**
     * 保存或修改供应商
     *
     * @param supplierId
     * @param supplier
     * @return
     */
    //http://localhost:8080/supplier/save?supplierId=1
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "supplierId", required = false) Integer supplierId,
                                  Supplier supplier) {
        if (supplierId != null) {
            supplierService.updateSupplier(supplierId, supplier);
        } else {
            supplierService.saveSupplier(supplier);
        }

        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    //http://localhost:8080/supplier/delete
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam String ids) {
        boolean delete = supplierService.delete(ids);
        if (delete) {
            return new  ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(605, "请求失败", null);
    }
}
