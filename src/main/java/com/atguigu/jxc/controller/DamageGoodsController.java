package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/12 8:30
 * @Version:1.0
 * @Description
 **/
@RestController
@RequestMapping("/damageListGoods")
public class DamageGoodsController {

    @Autowired
    DamageGoodsService damageGoodsService;


    //http://localhost:9999/damageListGoods/save?damageNumber=BS1694478526600（报损单号,前端生成）
    @PostMapping("/save")
    public ServiceVO damageListGoods(DamageList damageList,
                                     String damageListGoodsStr,
                                     HttpSession request) {

        User user = (User) request.getAttribute("currentUser");
        Integer userId = user.getUserId();
        Boolean save = damageGoodsService.saveDamageList(damageList, damageListGoodsStr, userId);
        if (save) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @param request
     * @return
     * @throws Exception
     */
    //http://localhost:8080/damageListGoods/list
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam String sTime,
                                    @RequestParam String eTime,
                                    HttpSession request) throws Exception {
        User user = (User) request.getAttribute("currentUser");
        Integer userId = user.getUserId();
        Map<String, Object> map = damageGoodsService.list(sTime, eTime, userId);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    //http://localhost:8080/damageListGoods/goodsList
    @PostMapping("/goodsList")
    public Map<String, Object> damageGoodsList(@RequestParam Integer damageListId) {
        Map<String, Object> map = damageGoodsService.damageGoodsList(damageListId);
        return map;
    }
}
