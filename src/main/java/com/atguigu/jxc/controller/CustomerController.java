package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 18:03
 * @Version:1.0
 * @Description
 **/
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;


    /**
     * 客户列表分页
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    //http://localhost:8080 /customer/list
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam(required = false) Integer page,
                                    @RequestParam(required = false) Integer rows,
                                    @RequestParam(required = false) String customerName) {
        Map<String, Object> map = customerService.list(page, rows, customerName);
        return map;
    }

    /**
     * 客户添加或修改
     * @param customerId
     * @param customer
     * @return
     */
    //http://localhost:8080/ customer/save?customerId=1
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "customerId", required = false) Integer customerId,
                                  Customer customer) {
        ServiceVO success = new ServiceVO(100, "请求成功", null);
        ServiceVO error = new ServiceVO(606, "请求失败", null);
        if (customerId != null) {
            Boolean update = customerService.updateCustomer(customerId, customer);
            if (update) {
                return success;
            }
            return error;
        }
        Boolean save = customerService.saveCustomer(customer);
        if (save) {
            return success;
        }
        return error;
    }

    /**
     * 客户删除
     * @param ids
     * @return
     */
    //http://localhost:8080/customer/delete
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam String ids) {
        Boolean delete = customerService.delete(ids);
        if (delete) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }

}
