package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/12 10:55
 * @Version:1.0
 * @Description
 **/
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {
    @Autowired
    OverflowListGoodsService overflowListGoodsService;

    //http://localhost:8080/overflowListGoods/save?overflowNumber=BY1605767033015
    @PostMapping("/save")
    public ServiceVO save(OverflowList overflowList,
                          String overflowListGoodsStr,
                          HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        Boolean save = overflowListGoodsService.saveOverListGoods(overflowList, overflowListGoodsStr, userId);
        if (save) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    //http://localhost:8080/overflowListGoods/list
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam String sTime,
                                    @RequestParam String eTime) {
        Map<String, Object> map = overflowListGoodsService.list(sTime, eTime);
        return map;
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    //http://localhost:8080/overflowListGoods/goodsList
    @PostMapping("/goodsList")
    public Map<String, Object> goodList(@RequestParam Integer overflowListId) {
        Map<String, Object> map = overflowListGoodsService.goodList(overflowListId);
        return map;
    }
}
