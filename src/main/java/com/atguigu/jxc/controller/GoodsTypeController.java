package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     *
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value = {"商品管理", "进货入库", "退货出库", "销售出库", "客户退货", "当前库存查询", "商品报损", "商品报溢", "商品采购统计"}, logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    /**
     * 新增分类
     *
     * @param goodsTypeName
     * @param pId
     * @return
     */
    //http://localhost:8080/goodsType/save
    @PostMapping("/save")
    public ServiceVO save(@RequestParam String goodsTypeName,
                          @RequestParam Integer pId) {
        Boolean save = goodsTypeService.save(goodsTypeName, pId);
        if (save) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }


    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    //http://localhost:8080/goodsType/delete
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam Integer goodsTypeId) {
        Boolean save = goodsTypeService.delete(goodsTypeId);
        if (save) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }
}
