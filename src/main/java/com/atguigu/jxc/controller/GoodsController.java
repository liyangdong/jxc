package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.vo.GoodsVo;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;




    /**
     * 分页查询当前库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    //http://localhost:8080/goods/listInventory
    @PostMapping("/listInventory")
    public Map<String, Object> listInventory(@RequestParam(required = false)  Integer page,
                                              @RequestParam(required = false)  Integer rows,
                                              @RequestParam(required = false)  String codeOrName ,
                                              @RequestParam(required = false) Integer goodsTypeId){
        Map<String, Object> map=goodsService.listInventory(page,rows,goodsTypeId,codeOrName);
        return map;
    }

    /**
     * 查询所有商品信息
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    //http://localhost:8080/goods/list
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam(required = false)  Integer page,
                                             @RequestParam(required = false)  Integer rows,
                                             @RequestParam(value = "goodsName",required = false)  String codeOrName ,
                                             @RequestParam(required = false) Integer goodsTypeId){
        Map<String, Object> map=goodsService.listInventory(page,rows,goodsTypeId,codeOrName);
        return map;
    }

    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    //http://localhost:8080/goods/save?goodsId=37
    @PostMapping("/save")
    public ServiceVO save(@RequestParam(value = "goodsId",required = false) Integer goodsId,
                          Goods goods){
        ServiceVO success = new ServiceVO(100, "请求成功", null);
        ServiceVO error = new ServiceVO(606, "请求失败", null);
        if (goodsId != null) {
            Boolean update = goodsService.updateGoods(goodsId, goods);
            if (update) {
                return success;
            }
            return error;
        }
        Boolean save = goodsService.saveGoods(goods);
        if (save) {
            return success;
        }
        return error;
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    //http://localhost:8080/goods/delete
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam Integer goodsId) {
        Boolean delete = goodsService.delete(goodsId);
        if (delete) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //http://localhost:8080/goods/getNoInventoryQuantity
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam(required = false) Integer page,
                                                     @RequestParam(required = false)Integer rows,
                                                     @RequestParam(required = false)String nameOrCode){
        Map<String, Object> map = goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //http://localhost:8080/goods/getHasInventoryQuantity
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(@RequestParam(required = false) Integer page,
                                                     @RequestParam(required = false)Integer rows,
                                                     @RequestParam(required = false)String nameOrCode){
        Map<String, Object> map = goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
        return map;
    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @return
     */
    //http://localhost:8080/goods/saveStock?goodsId=25
    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam Integer goodsId,
                                  @RequestParam Integer inventoryQuantity,
                                  @RequestParam double purchasingPrice) {
        Boolean update = goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        if (update) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }

    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    //http://localhost:8080/goods/deleteStock
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(@RequestParam Integer goodsId) {
        Boolean delete = goodsService.deleteStock(goodsId);
        if (delete) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(606, "请求失败", null);
    }


    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    //http://localhost:8080/goods/listAlarm
    @PostMapping("/listAlarm")
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = goodsService.listAlarm();
        return map;

    }
}
