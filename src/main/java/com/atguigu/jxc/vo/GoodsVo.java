package com.atguigu.jxc.vo;

import com.atguigu.jxc.entity.Goods;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 10:16
 * @Version:1.0
 * @Description
 **/
@NoArgsConstructor
@Data
public class GoodsVo {
    private Integer total;
    private List<Goods> rows;

}
