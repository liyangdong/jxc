package com.atguigu.jxc.vo;

import com.atguigu.jxc.entity.Supplier;
import lombok.Data;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 15:16
 * @Version:1.0
 * @Description
 **/
@Data
public class SupplierVo {
    private Integer total;
    private List<Supplier> rows;
}
