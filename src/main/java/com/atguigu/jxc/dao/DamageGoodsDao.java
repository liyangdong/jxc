package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/12 8:32
 * @Version:1.0
 * @Description
 **/
public interface DamageGoodsDao {


    /**
     * 保存报修单
     * @param damageList
     * @param userId
     * @return
     */
    Boolean saveDamageList(@Param("damageList") DamageList damageList, @Param("userId") Integer userId);

    /**
     * 保存报修商品
     * @param listGoods
     * @return
     */
    Boolean saveDamageGoods(@Param("listGoods") List<DamageListGoods> listGoods);


    /**
     * 查询报损单商品信息
     *
     * @param damageListId
     * @return
     */
    List<DamageListGoods> damageGoodsList(Integer damageListId);
}
