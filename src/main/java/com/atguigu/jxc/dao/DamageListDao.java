package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/12 14:11
 * @Version:1.0
 * @Description
 **/
public interface DamageListDao {

    /**
     * 报损单查询
     *
     * @return
     */
    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime, @Param("userId") Integer userId);


}
