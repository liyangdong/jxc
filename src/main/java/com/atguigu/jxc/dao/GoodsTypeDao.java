package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    /**
     * 新增分类
     *
     * @param goodsTypeName
     * @param pId
     * @param goodsTypeState
     * @return
     */
    Boolean save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("goodsTypeState") int goodsTypeState);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    Boolean delete(Integer goodsTypeId);

    /**
     * 获取state状态
     * @param pId
     * @return
     */
    int getGoodsTypeState(Integer pId);

    void updateTypeState(Integer pId, Integer state);

    /**
     * 查询父节点的id
     * @param goodsTypeId
     * @return
     */
    int getParentTypeId(Integer goodsTypeId);

    /**
     * 获取父节点下的字节点
     * @param pId
     * @return
     */
    List<Integer> getChildTypeId(int pId);
}
