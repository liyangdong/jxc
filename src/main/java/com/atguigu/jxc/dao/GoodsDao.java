package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();



    /**
     * 获取商品总数
     *
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
    int getGoodsTotal(@Param("goodsTypeId") Integer goodsTypeId,
                      @Param("codeOrName") String codeOrName);

    /**
     * 分页查询商品信息
     * @param offset
     * @param rows
     * @param goodsTypeId
     * @param codeOrName
     * @return
     */
//    List<Goods> listInventory(int offset, Integer rows, Integer goodsTypeId, String codeOrName);

    /**
     * 添加商品
     * @param goods
     * @return
     */
    Boolean saveGoods(Goods goods);

    /**
     * 更新商品
     * @param goodsId
     * @param goods
     * @return
     */
    Boolean updateGoods(@Param("goodsId") Integer goodsId, @Param("goods") Goods goods);

    /**
     * 删除商品
     * @param goodsId
     * @return
     */
    Boolean delete(Integer goodsId);


    /**
     * 无库存商品总记录数
     * @param nameOrCode
     * @return
     */
    int getNoStockTotal(String nameOrCode);

    /**
     * 无库存商品列表
     * @param offset
     * @param rows
     * @param nameOrCode
     * @return
     */
    List<Goods> getNoStockGoods(@Param("offset") int offset, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    /**
     * 库存商品有记录的总记录数
     * @param nameOrCode
     * @return
     */
    int getHasStockTotal(String nameOrCode);

    /**
     * 有库存商品列表
     * @param offset
     * @param rows
     * @param nameOrCode
     * @return
     */
    List<Goods> getHasInventoryQuantity(@Param("offset") int offset, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    Boolean saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    /**
     * 删除库存
     * @param goodsId
     * @return
     */
    Boolean deleteStock(Integer goodsId);

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    List<Goods> listAlarm();


    /**
     * 获取商品的类型状态
     * @param goodsTypeId
     * @return
     */
    int getGoodsTypeState(Integer goodsTypeId);

    /**
     * 获取父节点下的字节点
     * @param goodsTypeId
     * @return
     */
    List<Integer> getGoodsChild(Integer goodsTypeId);

    List<Goods> listInventoryChilds(@Param("offset") int offset, @Param("rows") Integer rows, @Param("idList") List<Integer> idList, @Param("codeOrName") String codeOrName);

    List<Goods> listInventory(@Param("offset") int offset, @Param("rows") Integer rows, @Param("goodsTypeId") Integer goodsTypeId, @Param("codeOrName") String codeOrName);
}
