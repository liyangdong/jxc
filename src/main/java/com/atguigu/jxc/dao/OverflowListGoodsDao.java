package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/12 11:00
 * @Version:1.0
 * @Description
 **/
public interface OverflowListGoodsDao {

    /**
     * 新增报溢单
     * @param overflowList
     * @param userId
     */
    void saveOverList(@Param("overflowList") OverflowList overflowList, @Param("userId") Integer userId);

    /**
     * 添加报溢商品
     * @param listGoods
     * @return
     */
    Boolean batchSave(List<OverflowListGoods> listGoods);

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @param userId
     * @return
     */
    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> goodList(Integer overflowListId);
}
