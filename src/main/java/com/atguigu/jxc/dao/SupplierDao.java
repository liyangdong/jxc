package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 15:12
 * @Version:1.0
 * @Description
 **/
public interface SupplierDao {

    /**
     * 根据条件获取供应商总的人数
     * @param supplierName
     * @return
     */
    int getTotal(String supplierName);

    /**
     * 根据条件获取供应商的数据
     * @param offset
     * @param rows
     * @param supplierName
     * @return
     */
    List<Supplier> getListSupplier(int offset, Integer rows, String supplierName);

    /**
     * 添加供应商
     * @param supplier
     */
    void saveSupplier(Supplier supplier);

    /**
     * 更新供应商
     * @param supplierId
     * @param supplier
     */
    void updateSupplier(@Param("supplierId") Integer supplierId, @Param("supplier") Supplier supplier);

    /**
     * 删除供应商
     * @param idList
     * @return
     */
    Boolean delete(List<String> idList);
}
