package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 18:01
 * @Version:1.0
 * @Description
 **/
public interface CustomerDao {

    /**
     * 获取客户列表总记录数
     * @param customerName
     * @return
     */
    int getTotal(String customerName);

    /**
     * 客户列表分页
     * @param offset
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getList(@Param("offset") int offset, @Param("rows") Integer rows, @Param("customerName") String customerName);


    /**
     * 更新
     * @param customerId
     * @param customer
     * @return
     */
    Boolean update(@Param("customerId") Integer customerId, @Param("customer") Customer customer);

    /**
     * 保存
     * @param customer
     * @return
     */
    Boolean save(Customer customer);

    /**
     * 删除
     * @param idList
     * @return
     */
    Boolean delete(List<String> idList);
}
