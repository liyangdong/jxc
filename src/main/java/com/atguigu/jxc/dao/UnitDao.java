package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * @Author:lyd
 * @Date:2023/9/11 19:36
 * @Version:1.0
 * @Description
 **/
public interface UnitDao {
    /**
     * 查询所有商品单位
     * @return
     */
    List<Unit> getUnit();

}
