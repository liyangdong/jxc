package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/12 8:31
 * @Version:1.0
 * @Description
 **/
public interface DamageGoodsService {

    /**
     * 保存商品报损单
     *
     * @param damageListGoodsStr
     * @param damageList
     * @param userId
     * @return
     */
    Boolean saveDamageList(DamageList damageList, String damageListGoodsStr, Integer userId);

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @param userId
     * @return
     */
    Map<String, Object> list(String sTime, String eTime, Integer userId) throws Exception;


    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    Map<String, Object> damageGoodsList(Integer damageListId);
}
