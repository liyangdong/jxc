package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 19:36
 * @Version:1.0
 * @Description
 **/
public interface UnitService {


    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> getUnit();
}
