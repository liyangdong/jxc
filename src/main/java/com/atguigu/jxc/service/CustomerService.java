package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 18:01
 * @Version:1.0
 * @Description
 **/
public interface CustomerService {

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);


    /**
     * 更新用户列表
     * @param customerId
     * @param customer
     * @return
     */
    Boolean updateCustomer(Integer customerId, Customer customer);


    /**
     * 保存用户列表
     * @param customer
     * @return
     */
    Boolean saveCustomer(Customer customer);

    /**
     * 客户删除
     * @param ids
     * @return
     */
    Boolean delete(String ids);
}
