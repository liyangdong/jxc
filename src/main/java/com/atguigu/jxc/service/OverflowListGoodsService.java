package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/12 11:00
 * @Version:1.0
 * @Description
 **/
public interface OverflowListGoodsService {

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param userId
     * @return
     */
    Boolean saveOverListGoods(OverflowList overflowList, String overflowListGoodsStr, Integer userId);

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> goodList(Integer overflowListId);
}
