package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 15:11
 * @Version:1.0
 * @Description
 **/
public interface SupplierService {
    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName);

    /**
     * 更新供应商数据
     * @param supplierId
     * @param supplier
     */
    void updateSupplier(Integer supplierId, Supplier supplier);

    /**
     * 添加供应商
     * @param supplier
     */
    void saveSupplier(Supplier supplier);

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    boolean delete(String ids);
}
