package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:lyd
 * @Date:2023/9/11 18:01
 * @Version:1.0
 * @Description
 **/
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        page = page == 0 ? 1 : page;
        int offset=(page-1)*rows;
        int total = customerDao.getTotal(customerName);
        List<Customer> customerList = customerDao.getList(offset, rows, customerName);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", customerList);
        return map;

    }

    @Override
    public Boolean updateCustomer(Integer customerId, Customer customer) {
        Boolean update = customerDao.update(customerId, customer);
        return update;
    }

    @Override
    public Boolean saveCustomer(Customer customer) {
        Boolean save = customerDao.save(customer);
        return save;
    }

    @Override
    public Boolean delete(String ids) {
        String[] split = ids.split(",");
        List<String> idList = new ArrayList<>();
        for (String s : split) {
            idList.add(s);
        }
        Boolean delete = customerDao.delete(idList);
        return delete;
    }
}
