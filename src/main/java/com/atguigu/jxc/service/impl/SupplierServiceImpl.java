package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author:lyd
 * @Date:2023/9/11 15:12
 * @Version:1.0
 * @Description
 **/
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    SupplierDao supplierDao;

    @Override
    public Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName) {
        int total = supplierDao.getTotal(supplierName);
        page = page == 0 ? 1 : page;
        int offset=(page-1)*rows;
        List<Supplier> supplierList = supplierDao.getListSupplier(offset, rows, supplierName);

        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", supplierList);
        return map;
    }

    @Override
    public void updateSupplier(Integer supplierId, Supplier supplier) {
        supplierDao.updateSupplier(supplierId, supplier);
    }

    @Override
    public void saveSupplier(Supplier supplier) {
        supplierDao.saveSupplier(supplier);
    }

    @Override
    public boolean delete(String ids) {
        String[] split = ids.split(",");
        List<String> idList = new ArrayList<>();
        Arrays.stream(split).forEach(item -> idList.add(item));
        Boolean delete = supplierDao.delete(idList);
        return delete;

    }

}
