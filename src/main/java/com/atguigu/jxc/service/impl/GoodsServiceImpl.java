package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.vo.GoodsVo;
import com.atguigu.jxc.service.GoodsService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, Integer goodsTypeId, String codeOrName) {
        Map<String, Object> goodsVoMap = new HashMap<>();
        int total = goodsDao.getGoodsTotal(goodsTypeId, codeOrName);
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        int goodsTypeSate = 0;

        List<Integer> idList;
        List<Goods> goodsList;
        if (goodsTypeId != null && goodsTypeId!=1) {
            goodsTypeSate = goodsDao.getGoodsTypeState(goodsTypeId);
        }
        if (goodsTypeSate != 0) {
            idList = goodsDao.getGoodsChild(goodsTypeId);
            goodsList = goodsDao.listInventoryChilds(offset, rows, idList, codeOrName);
            goodsVoMap.put("total", total);
            goodsVoMap.put("rows", goodsList);
            return goodsVoMap;
        }else {
            if (new Integer(1).equals(goodsTypeId)) {
                goodsTypeId=null;
            }
            goodsList = goodsDao.listInventory(offset, rows, goodsTypeId, codeOrName);
            goodsVoMap.put("total", total);
            goodsVoMap.put("rows", goodsList);
            return goodsVoMap;
        }
    }

    @Override
    public Boolean updateGoods(Integer goodsId, Goods goods) {
        Boolean update = goodsDao.updateGoods(goodsId, goods);
        return update;

    }

    @Override
    public Boolean saveGoods(Goods goods) {
        Boolean save = goodsDao.saveGoods(goods);
        return save;
    }

    @Override
    public Boolean delete(Integer goodsId) {

        Boolean delete = goodsDao.delete(goodsId);
        return delete;
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        int total = goodsDao.getNoStockTotal(nameOrCode);
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoStockGoods(offset, rows, nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        int total = goodsDao.getHasStockTotal(nameOrCode);
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offset, rows, nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Boolean saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Boolean update = goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        return update;
    }

    @Override
    public Boolean deleteStock(Integer goodsId) {
        Boolean delete = goodsDao.deleteStock(goodsId);
        return delete;
    }


    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> goodsList = goodsDao.listAlarm();
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        return map;

    }


}
