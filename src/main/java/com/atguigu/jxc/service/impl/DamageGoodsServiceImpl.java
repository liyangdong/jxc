package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageGoodsDao;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author:lyd
 * @Date:2023/9/12 8:31
 * @Version:1.0
 * @Description
 **/
@Service
public class DamageGoodsServiceImpl implements DamageGoodsService {
    @Autowired
    DamageGoodsDao damageGoodsDao;
    @Autowired
    DamageListDao damageListGoodsDao;
    @Transactional
    @Override
    public Boolean saveDamageList(DamageList damageList, String damageListGoodsStr, Integer userId) {
       Boolean save= damageGoodsDao.saveDamageList(damageList,userId);
        Gson gson = new Gson();
        Type type = new TypeToken<List<DamageListGoods>>(){}.getType();
        List<DamageListGoods> damageListGoods = gson.fromJson(damageListGoodsStr, type);
        List<DamageListGoods> listGoods = damageListGoods.stream()
                .map(item -> {
                    item.setDamageListId(damageList.getDamageListId());
                    return item;
                })
                .collect(Collectors.toList());
        Boolean saveDamageGoods=damageGoodsDao.saveDamageGoods(listGoods);
        return saveDamageGoods ;
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime, Integer userId) throws Exception {

        List<DamageList> goodsList = damageListGoodsDao.list(sTime,eTime,userId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> damageGoodsList(Integer damageListId) {
        List<DamageListGoods> goodsList = damageGoodsDao.damageGoodsList(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        return map;
    }
}
