package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author:lyd
 * @Date:2023/9/12 11:00
 * @Version:1.0
 * @Description
 **/
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    OverflowListGoodsDao overflowListGoodsDao;

    @Transactional
    @Override
    public Boolean saveOverListGoods(OverflowList overflowList, String overflowListGoodsStr, Integer userId) {
        overflowListGoodsDao.saveOverList(overflowList,userId);
        Gson gson = new Gson();
        Type type= new TypeToken<List<OverflowListGoods>>(){}.getType();
        List<OverflowListGoods> overflowListGoods = gson.fromJson(overflowListGoodsStr, type);
        List<OverflowListGoods> listGoods = overflowListGoods.stream()
                .map(item -> {
                    item.setOverflowListId(overflowList.getOverflowListId());
                    return item;
                })
                .collect(Collectors.toList());
        Boolean save = overflowListGoodsDao.batchSave(listGoods);
        return save;
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {


        List<OverflowList> goodsList =  overflowListGoodsDao.list(sTime, eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> goodList(Integer overflowListId) {
        List<OverflowListGoods> goodsList =  overflowListGoodsDao.goodList(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        return map;
    }
}
