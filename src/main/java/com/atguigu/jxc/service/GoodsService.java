package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.vo.GoodsVo;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> listInventory(Integer page, Integer rows, Integer goodsTypeId, String codeOrName);


    /**
     * 更新商品
     * @param goodsId
     * @param goods
     * @return
     */
    Boolean updateGoods(Integer goodsId, Goods goods);

    /**
     * 保存商品
     * @param goods
     * @return
     */
    Boolean saveGoods(Goods goods);


    /**
     * 删除商品
     * @param goodsId
     * @return
     */
    Boolean delete(Integer goodsId);

    /**
     * 无库存商品列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);


    /**
     * 有库存商品列表展示
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */

    Boolean saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除库存
     * @param goodsId
     * @return
     */
    Boolean deleteStock(Integer goodsId);

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    Map<String, Object> listAlarm();

}
